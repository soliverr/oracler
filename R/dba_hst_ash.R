# Active Session History
#

dba_hst_ash <- function(dbid = NULL, instance_number = NULL, start_time = NULL, end_time = NULL)
  paste0(
    "select *
       from dba_hist_active_sess_history
      where dbid = ", ifelse( is.null(dbid),
                              ifelse( is.null(default_DBID()$dbid),
                                      "dbid",
                                      default_DBID()$dbid
                              ),
                              dbid
                      ), "
        and instance_number = ", ifelse( is.null(instance_number), default_DBID()$instance_number, instance_number ),
    ifelse( is.null(start_time),
       "",
       paste0("and cast(sample_time as date) >= to_date('", start_time, "', '", datetime_fmt(), "')")
    ),
    ifelse( is.null(end_time),
            "",
            paste0("and cast(sample_time as date) <= to_date('", end_time, "', '", datetime_fmt(), "')")
    )
  )

hst_ash_modules <- function(dbid = NULL, instance_number = NULL, start_time = NULL, end_time = NULL)
  paste0("
    with ash as (",
      dba_hst_ash( dbid, instance_number, start_time, end_time), "
    )
    select dbid, instance_number, max(snap_id) as end_snap, nvl(module, 'NULL') as module, min(trunc(sample_time, 'HH24')) as sample_time, count(1) as val
     from ash
    group by dbid, instance_number, trunc(sample_time, 'HH24'), module
    order by dbid, instance_number, sample_time
  ")

# Total number of sessions
hst_ash_sessions <- function(dbid = NULL, instance_number = NULL, start_time = NULL, end_time = NULL)
  paste0("
    with ash as (",
         dba_hst_ash( dbid, instance_number, start_time, end_time), "
    )
    select dbid, instance_number, snap_id as snap_end, sample_time, sum(num_session) as num_session
     from (
           select dbid, instance_number, snap_id, trunc(sample_time, 'MI') as sample_time, session_id, session_serial#, 1 as num_session
             from ash
            group by dbid, instance_number, snap_id, trunc(sample_time, 'MI'), session_id, session_serial#
          )
    group by dbid, snap_id, sample_time
    order by dbid, instance_number, sample_time
  ")
